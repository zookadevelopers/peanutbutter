# Skippy

### 1. Clone repository
- SSH
```git clone git@bitbucket.org:zookadevelopers/skippy.git```

Checkout to develop branch
```git checkout develop```

### 2. Build image
Run in main directory
```docker build -t skippy ./```

### 3. Run container
`docker run -ti --name skippy -p 80:80 -p 3306:3306 -v $(PWD):/var/www/html -v ~/docker/skippy:/var/lib/mysql skippy`

### 4. Run Services
With the following code line you can run mysql, ngnix services
`/runner/./local.sh`

### 5. Database
Update the ip/dns in the backup file of the database.

`replace "skippy.zookastagging.com" "localhost" -- admin_skippy.sql`

Login to mysql with your username and password `mysql -u root -p` and in the mysql console type the following command` create database skippy`, logout to mysql, then import the database `mysql skippy <  [DATABASE-BACKUP-DIRECTORY]`.