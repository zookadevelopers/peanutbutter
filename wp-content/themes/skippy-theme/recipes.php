<?php 
// Template Name: Recipes

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$search_filter_id = get_field( 'search_and_filter_id', 'option' );

// Products Query
$recipes = new WP_Query(
	array(
		'post_type' 		=> 'post',
		'search_filter_id'  => $search_filter_id, 
		'post_status' 		=> 'publish'
	)
);
?>

<?php
	global $page_image;
	$banner_value = get_field('recipes_page_banner', 'option');

	if( $banner_value ) {
		$page_image = $banner_value;
	} else {
		$page_image = $upload_theme.'recipe_langing_page_banner.jpg';
	}
?>

<?php get_header(); ?>

<section class="banner" style="background-image: url(<?php echo $page_image; ?>)">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="page-title">All recipes</h1>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->	
	</div><!-- /.container -->
</section><!-- /.banner -->

<section class="filter-section d-block d-md-none">
	<div class="container">
		<div class="ssm-toggle-nav title-filters">
			<h2>Filter all recipes</h2>	
		</div><!-- /.ssm-toggle-nav hamburger-menu -->
	</div><!-- /.container -->
</section><!-- /.filter-section d-block d-md-none -->

<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-md-4 col-lg-3 left-filters d-none d-md-block">
			<?php get_template_part( 'template-parts/category-filters' ); ?>
		</div><!-- /.col-12 col-lg-3 -->
		<div class="col-12 col-md-8 col-lg-9 col-xl-8 recipes-container list-recipes right-recipes" id="main-recipes">
			<div class="container">
				<div class="row">
					<?php while ( $recipes->have_posts() ) : $recipes->the_post(); global $post; ?>
						<?php
							$pageId = get_field('page_id');
							$name 	= get_the_title();
							$imgUrl = get_the_post_thumbnail_url();
							$currentUrl = get_permalink();
							$post_slug 	= get_post_field( 'post_name', get_post() );


							$objPowerReviews[] = [
								'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
								'locale' => 'en_US',
								'merchant_group_id' => '78292',
								'merchant_id' => '146622',
								'page_id' => strval($pageId),
								'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
								'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
								'components' => [
									'CategorySnippet' => 'pr-reviewsnippet-recipe-'.$pageId
								]
							];
						?>
					<?php require "template-parts/recipe-item.php"; ?>
					<?php endwhile; wp_reset_postdata(); ?>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.col-12 col-lg-9 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<a class="back-top">
	<figure>
		<img class="img-fluid" src="<?php echo $upload_theme; ?>back-to-top.png" width="auto" height="auto" alt="Back to Top" />
	</figure>
</a>
<script type="text/javascript" charset="utf-8">
	
		jQuery(window).load(function(){
			(function(){ try {
			POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
			}catch(e){window.console && window.console.log(e)}}());

		});
	
</script>
<?php get_footer(); ?>