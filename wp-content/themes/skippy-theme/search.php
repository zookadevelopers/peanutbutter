<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */
	
	$cont_products = 0;
	$cont_recipies = 0;
	$total_results = 0;

	$products = new WP_Query(
		array(
			'post_type'=>'products', 
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	);

	$recipes = new WP_Query(
		array(
			'post_type'=>'post', 
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	);


while ( have_posts() ) : the_post(); 
	$posType = get_post_type();

	// View products by category
	$one_product = new WP_Query(
		array(
			'post_type'=>$posType, 
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	);

	/*
		Validate is result is a products in category
		Product == category
	*/
	if ($posType != 'post' && $posType != 'page' && $posType != 'products') {
		while ( $one_product->have_posts() ) : $one_product->the_post();
			$cont_products ++;
		endwhile;
	} else if ($posType == 'post') {
		$cont_recipies ++;
	}
	
endwhile;
	$total_results = $cont_products + $cont_recipies;

get_header(); ?>

	<section>
		<div class="container-fluid search-page-title">
			<div class="row">
				<div class="col-12">
					<?php
					echo '<p><span>'.$total_results.'</span> Results for <span>'.'"'.get_search_query().'"'.'</span></p>';
					?>
				</div>
			</div>
		</div>
	</section>


	<section id="primary" class="container search-result">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<div class="row">
				<div id="accordion" class="accordion-content">
					<ul class="nav nav-tabs nav-custom">
						<li class="nav-item">
							<div id="product-results">
								<button class="btn btn-link" data-toggle="collapse" data-target="#collapseProduct" aria-expanded="true" aria-controls="collapseProduct">Products <?php echo '('.$cont_products.')' ?></button>
							</div>
						</li>
						<li class="nav-item">
							<div id="recipes-results">
								<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseRecipies" aria-expanded="false" aria-controls="collapseRecipies">Recipies <?php echo '('.$cont_recipies.')' ?></button>
							</div>
						</li>
					</ul>

				    <div id="collapseProduct" class="collapse show" aria-labelledby="product-results" data-parent="#accordion">
				      <div class="card-body">
				      	
				      	<?php 
							while ( have_posts() ) : the_post(); 
								$posType = get_post_type();
								$posSlug = get_field('post_type_slug');
								$posSlug = str_replace(" ","_",$posSlug);

								// View products by category
								$one_product = new WP_Query(
									array(
										'post_type'=>$posType, 
										'post_status'=>'publish', 
										'posts_per_page'=> -1,
										'order' => 'ASC',
										'orderby'=>'menu_order'
									)
								);

								/*
									Validate is result is a products in category
									Product == category
								*/
								if ($posType != 'post' && $posType != 'page' && $posType != 'products') {
									while ( $one_product->have_posts() ) : $one_product->the_post();
										$posType = get_post_type();
										get_template_part( 'template-parts/content-search', get_post_format() );
									endwhile;
								}

							endwhile;
						?>

				      </div>
				    </div>

				    <div id="collapseRecipies" class="collapse" aria-labelledby="recipes-results" data-parent="#accordion">
				      <div class="card-body">
				      	<?php
				      		while ( have_posts() ) : the_post();
				      			$posType = get_post_type();
				      			if ($posType == 'post') {
									get_template_part( 'template-parts/content-search', get_post_format() );
								}
							endwhile;
				      	?>
				      </div>
				    </div>

				</div>
		<?php
					
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
			
			</div><!-- /.row -->
		</main><!-- #main -->
	</section><!-- #primary -->


<?php
get_footer();
