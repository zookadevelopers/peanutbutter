<?php 
// Template Name: Products

// Products Home
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'DESC'
	)
); 

$posInfo = get_post();
// var_dump($posInfo);
$posName = get_field('post_type_slug');
$posName = str_replace(" ","_",$posName);

$categoryName = str_replace("_","-",$posName);
// Products Query
$products = new WP_Query(
	array(
		'post_type'=>$posName, 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC'
	)
);

// Recipes query
$recipes = new WP_Query(
	array(
		'post_type'=>'post', 
		'post_status'=>'publish', 
		'posts_per_page'=> 4,
		'order' => 'ASC'
	)
); 

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>


<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php
	global $category_image;
	global $category_image_mobile;
	global $device;

	$banner_value = get_field('category_banner_image');
	$banner_value_mobile = get_field('category_banner_image_mobile');

	if( $banner_value ) {
		$category_image = $banner_value;
	} else {
		$category_image = $upload_theme.'banner-products.jpg';
	}

	if( $banner_value_mobile ) {
		$category_image_mobile = $banner_value_mobile;
	} else {
		$category_image_mobile = $upload_theme.'banner-products.jpg';
	}
	?>

<?php 
	if ( wp_is_mobile() ) {
		$device = $category_image_mobile;
	} else {
		$device = $category_image;
	}
 ?>

<section class="banner-product banner-product-info container-fluid <?php echo $categoryName; ?>" style="background-image: url('<?php echo $device; ?>');">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-8">
				<h1><?php the_field('title_category'); ?></h1>
				<p><?php the_field('category_description'); ?></p>
			</div>
			<figure class="product-banner">
				<img src="<?php the_field('product_banner'); ?>" alt="" class="img-fluid">
			</figure>
		</div><!-- /.row -->
	</div>
</section>

<section class="products-home product-category single-products reviews-single <?php echo $categoryName; ?>">
	<div class="container">
		<div class="row items-products">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<?php
					$pageId = get_field('page_id');
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
						'locale' => 'en_US',
						'merchant_group_id' => '78292',
						'merchant_id' => '146622',
						'page_id' => strval($pageId),
						'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
						'components' => [
							'ReviewSnippet' => 'pr-reviewsnippet-'.$pageId
						]
					];
				?>
				<?php get_template_part( 'template-parts/product-item' ); ?>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php 
	$posts = get_field('selected_recipes');
	if( $posts ):
?>
<?php if ($categoryName != 'pb-jelly-minis'): ?>
	<section class="cont-recipes list-recipes">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>Recipes with <?php the_field('title_category'); ?></h2>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			<div class="row recipes justify-content-center">
			<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php setup_postdata($post); ?>
				<?php
					$pageId = get_field('page_id');
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
						'locale' => 'en_US',
						'merchant_group_id' => '78292',
						'merchant_id' => '146622',
						'page_id' => strval($pageId),
						'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-recipe-'.$pageId
						]
					];
				?>
				<?php get_template_part( 'template-parts/recipe-item' ); ?>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.cont-recipes -->
<?php endif; ?>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php endwhile; wp_reset_postdata(); else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
