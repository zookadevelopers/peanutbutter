<?php 
// Template Name: Write Review
	get_header(); 
?>
<?php
	$postId = $_GET['post_id'];
	$postInfo = get_post($postId);
	$pageId = get_field('page_id', $postId);

	$postTitle = get_field('product-name' , $postId);
	$postDescription = get_field('product-description', $postId);
	$postImage = get_the_post_thumbnail_url($postId);
	$postUrl = get_post_permalink( $postId );
	$postExcerpt = get_the_excerpt( $postId );
?>

<?php 
	$allposttags = get_the_tags($postId);
	$i=0;
	if ($allposttags) {
		foreach($allposttags as $tags) {
			$i++;
			if (1 == $i) {
				$firsttag = $tags->name;
			}
		}
	}
?>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12">
			<div id="pr-write" class="cont-write-review"></div>
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<script>// <![CDATA[
	var currentUrl = window.location.host +'/?page_id='+<?php echo $postId; ?>;
	var templateUrl = '<?= get_bloginfo("template_url"); ?>';

	POWERREVIEWS.display.render({
		api_key: '7783bde7-0813-4233-8474-9c2889af0d40',
		locale: 'en_US',
		merchant_group_id: '78292',
		merchant_id: '146622',
		page_id: "<?php echo $pageId; ?>",
		style_sheet: templateUrl+'/inc/assets/css/custom/reviews.css',
		product:{
			name: "<?php echo $postTitle; ?>",
			url: "<?php echo $postUrl; ?>",
			description: "<?php echo utf8_decode($postExcerpt); ?>",
			image_url: "<?php echo $postImage; ?>",
			category_name: "Product",
			brand_name: "Skippy",
		},
		components: {
			Write: 'pr-write'
		}
	});
// ]]></script>


<?php get_footer(); ?>