<form role="search" method="get" class="search-form main-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="d-flex align-items-center justify-content-center">
        <input type="search" oninput="this.value=this.value.replace(/[^A-Za-z0-9 ]/g,'');" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" class="input-search">

        <button type="submit" class="search-submit btn" value="<?php echo esc_attr_x( 'Search', 'submit button', 'wp-bootstrap-starter' ); ?>"><i class="fas fa-search"></i></button>
    </div>
</form>
