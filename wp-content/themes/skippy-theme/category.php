<?php 
// Template Name: Category

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

$category = get_the_category(); 

$posName = get_field('post_type_slug');
$posName = str_replace(" ","_",$posName);

// Products Query
$recipes_query = new WP_Query(
	array(
		'post_type'=> 'post', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'category_name'  => $category,
	)
);
?>

<?php get_header(); ?>

<section class="recipes-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 col-md-4 col-lg-3 left-filters d-none d-md-block">
				<?php get_template_part( 'template-parts/category-filters' ); ?>
			</div><!-- /.col-12 col-lg-3 -->
			<div id="recipe-results" class="col-12 col-md-8 col-lg-9 col-xl-8 recipes-container list-recipes right-recipes">
				<div class="container">
					<div class="row">
						<div class="col-12 text-center">
							<?php global $wp_query; ?>
							<div class="title-results">
								<h4><span><?php echo $wp_query->found_posts; ?> </span>Recipe results</h4>
								<p class="search-categories"></p><!-- /.search-categories -->
							</div><!-- /.title-results -->
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
					<?php if ( have_posts() ) : ?>
					<div class="row">
					<?php while ( have_posts() ) : the_post(); ?>
					<?php
						$pageId = get_the_ID();
						$name = get_the_title();
						$imgUrl = get_the_post_thumbnail_url();
						$currentUrl = get_permalink();

						$objPowerReviews[] = [
							'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
							'locale' => 'en_US',
							'merchant_group_id' => '78292',
							'merchant_id' => '146622',
							'page_id' => strval($pageId),
							'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
							'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
							'components' => [
								'CategorySnippet' => 'pr-reviewsnippet-recipe-'.$pageId
							]
						];
					?>
						<?php get_template_part( 'template-parts/recipe-item' ); ?>
					<?php endwhile; wp_reset_postdata(); ?>
					</div><!-- /.row -->
					<?php else: ?>
					<div class="row">
						<div class="col-12 text-center">
							<p>Sorry no Results! nothing matched your filter terms.</p>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
					<?php endif; ?>
				</div><!-- /.container -->
			</div><!-- /.col-12 col-lg-9 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.cont-recipes -->
<?php if ( have_posts() ) : ?> ?>
<script type="text/javascript" charset="utf-8">
	jQuery(window).load(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
<?php endif; ?>

<?php get_footer(); ?>