<?php
    $upload_dir = wp_upload_dir();
    $upload_theme = get_template_directory_uri().'/inc/assets/img/';
?>
<div id="footer-banner" class="footer-banner">
    <div class="container">
        <div class="row align-items-center">
            <div id="url-site" class="col-12 col-md-4 col-lg-2 url-site">
                <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo_footer' ) ): ?>
                    <a href="<?php echo esc_url( home_url( '/' )); ?>">
                        <img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo_footer' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" class="img-fluid" />
                    </a>
                <?php else : ?>
                    <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>">
                        <img src="<?php echo $upload_theme; ?>logo.png" class="img-fluid" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                    </a>
                <?php endif; ?>
                    
            </div>
            <div id="url-description" class="col col-md-8 col-lg-6 url-description"><?php dynamic_sidebar('footer-banner-2'); ?>
            </div>
        </div>
    </div>
</div>

<div class="cont-main-footer">
<?php
if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) {?>
        <div id="footer-widget" class="footer-widget">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'footer-1' )) : ?>
                        <div class="col-12 col-md-3 social-links-section"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                    <?php endif; ?>

                    <div class="col-12 col-md-3 nav-middle offset-lg-1">
                        <section id="custom_html-2" class="widget_text widget widget_custom_html">
                            <h3 class="widget-title">Follow us</h3>
                            <div class="textwidget custom-html-widget">
                                <div class="social-links-content">
                                    <ul>
                                        <li>
                                            <a href="https://www.facebook.com/Skippy" target="blank">
                                                <figure>
                                                    <img class="img-fluid" src="<?php echo $upload_theme ?>/global/facebook.svg" alt="social" width="auto" height="auto" />
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/Skippy" target="blank">
                                                <figure>
                                                    <img class="img-fluid" src="<?php echo $upload_theme ?>/global/twitter.svg" alt="social" width="auto" height="auto" />
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.pinterest.com/skippybrand/" target="blank">
                                                <figure>
                                                    <img class="img-fluid" src="<?php echo $upload_theme ?>/global/pinterest.svg" alt="social" width="auto" height="auto" />
                                                </figure>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/skippybrand" target="blank">
                                                <figure>
                                                    <img class="img-fluid" src="<?php echo $upload_theme ?>/global/instagram.svg" alt="social" width="auto" height="auto" />
                                                </figure>
                                            </a>
                                        </li>
                                    </ul>

                                    <figure class="smart-label">
                                        <a href="http://www.smartlabel.org/" target="_blank">
                                            <img class="img-fluid" src="<?php echo $upload_theme ?>/global/smartlabel.png" alt="smartlabel" width="auto" height="auto" />
                                        </a>
                                    </figure>
                                </div><!-- /.social-links-content -->
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
<?php }