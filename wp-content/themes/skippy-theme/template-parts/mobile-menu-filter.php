<div class="d-block d-md-none">
	<?php 
		$upload_dir = wp_upload_dir();
		$upload_theme = get_template_directory_uri().'/inc/assets/img/';
	?>

	<div class="ssm-overlay-filter ssm-toggle-nav-filter"></div>
	<div class="cont-mobile-menu left-filters">
		<a href="javascript:;" class="btn-close-menu-mobile ssm-toggle-nav-filter"><figure><img src="<?php echo $upload_theme ?>close.png" class="img-fluid" width="auto" height="auto" /></figure></a>

		<section class="menu-scroll">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php get_template_part( 'template-parts/category-filters' );?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.menu-scroll -->
	</div><!-- /.cont-mobile-menu -->
</div><!-- /.d-block d-sm-none -->
