<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if ( 'post' === get_post_type() && !is_single() ) : ?>

	<div class="col-lg-3 col-12">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

			<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
				<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
					<?php the_post_thumbnail(); ?>
				</div>
				<header class="entry-header">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;

					?>
				</header><!-- .entry-header -->

				<footer class="entry-footer">
					<?php wp_bootstrap_starter_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</a>
		</article><!-- #post-## -->

	</div>

<?php endif; ?>

<?php if (is_single()) : ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="container">
			<div class="row">
				<div class="col-12">
					<header class="entry-header">
						<h1 class="recipe-title"><?php the_field('sals-recipe-title'); ?></h1>
						
						<div class="recipe-reviews d-none">
							<div class="star-rating"></div><!-- /.star-rating -->
							<div class="write-review">
								<a href="#" class="btn btn-link-dark btn-review btn-recipe-gray">Write a Review</a>
							</div><!-- /.write-review -->
						</div><!-- /.recipe-reviews -->

						<div class="recipe-description">
							<p><?php the_field('sals-recipe-description'); ?></p>
							<div class="cont-show-more">
								<a href="#" class="btn btn-link read-more">Read More</a>
							</div>
						</div><!-- /.recipe-description -->

						<div class="recipe-information">
							<div class="recipe-servings information-item">
								<label>Servings</label>
								<span><?php the_field('sals-recipe-servings'); ?></span>
							</div><!-- /.recipe-servings -->
							<div class="recipe-calories information-item">
								<label>Calories/serving</label>
								<span><?php the_field('sals-recipe-calories-servings'); ?></span>
							</div><!-- /.recipe-calories -->
							<div class="recipe-prep-time information-item">
								<label>Prep Time</label>
								<span><?php the_field('sals-recipe-prep-time'); ?></span>
							</div><!-- /.recipe-prep-time -->
							<div class="recipe-cook-time information-item">
								<label>Cook Time</label>
								<span><?php the_field('sals-recipe-cook-time'); ?></span>
							</div><!-- /.recipe-cook-time -->
							<div class="recipe-total-time information-item">
								<label>Total Time</label>
								<span><?php the_field('sals-recipe-total-time'); ?></span>
							</div><!-- /.recipe-total-time -->

						</div><!-- /.recipe-information -->

					</header><!-- .entry-header -->
				</div><!-- /.col-12 -->
			</div><!-- /.row -->

			<div class="cont-columns">
				<div class="row">
					<div class="col-lg-4 col-12">
						<div class="column col-left col-ingredients">
							<h4>Ingredients</h4>
							<?php the_content(); ?>
						</div><!-- /.col-left col-ingredients -->
					</div><!-- /.col-lg-4 col-12 -->
					<div class="col-lg-8 col-12">
						<div class="column col-right col-directions">
							<h4>Directions</h4>
							<?php the_field('sals-recipe-directions'); ?>
						</div><!-- /.col-right col-directions -->
					</div><!-- /.col-lg-8 col-12 -->
				</div><!-- /.row -->
			</div><!-- /.cont-columns -->

			<div class="recipe-share">
				<div class="row">
					<div class="col-12">
						<?php echo do_shortcode('[ssba-buttons]'); ?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.recipe-share -->

			<div class="cont-reviews">
				<div class="row">
					<div class="col-12">
						<h2>Reviews</h2>
						<hr />
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
				
				<div class="row">
					<div class="col-5">
						<div id="pr-reviewsnippet"></div>
						<div id="pr-imagesnippet"></div>
						<div id="pr-reviewimagedisplay"></div>
					</div><!-- /.col-5 -->
					<div class="col-7">
						<div id="pr-reviewdisplay"></div>
					</div><!-- /.col-7 -->
				</div><!-- /.row -->

			</div><!-- /.cont-reviews -->

		</div><!-- /.container -->
		
		<?php if (get_field('sals-recipe-video')): ?>
			<!-- Modal -->
			<div class="modal fade recipe-video" id="recipeVideo" tabindex="-1" role="dialog" aria-labelledby="recipeVideoTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
		                    <div class="cont-popup-recipe-video">
		                        <?php the_field('sals-recipe-video') ?>
		                    </div><!-- /.cont-recipe-video -->
	                
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>

	</article><!-- #post-## -->
	<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
	<script type="text/javascript" charset="utf-8">
		var currentUrl = window.location.href;

		POWERREVIEWS.display.render({
			api_key: '2f3b17d7-519c-4be4-9501-402a0648e326',
			locale: 'en_US',
			merchant_group_id: '78361',
			merchant_id: '935948',
			page_id: '<?php echo get_the_ID(); ?>',
			review_wrapper_url: currentUrl + '?pr_page_id=__PAGE_ID__',
			product:{
				name: '<?php the_field('sals-recipe-title'); ?>', 
				url: currentUrl,
				image_url: '<?php the_post_thumbnail_url(); ?>',
				description: '<?php the_field('sals-recipe-description'); ?>',
				category_name: '<your_product_category_name>',
				manufacturer_id: '<your_product_manufacturer_id>',
				upc: '<your_product_upc>',
				brand_name: '<your_product_brand_name>',
				price: '<your_product_price>',
				in_stock: '<product_in_stock_status>',
			},
			components: {
				ReviewSnippet: 'pr-reviewsnippet',
				ReviewImageSnippet: 'pr-imagesnippet',
				ReviewDisplay: 'pr-reviewdisplay'
			}
		});
	</script>

<?php endif; ?>

<?php if ( is_page_template('search.php')): ?>

	<div class="col-lg-3 col-12">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

			<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
				<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
					<?php the_post_thumbnail(); ?>
				</div>
				<header class="entry-header">
					<?php
					if ( is_single() ) :
						the_title( '<h1 class="entry-title">', '</h1>' );
					else :
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					endif;

					?>
				</header><!-- .entry-header -->

				<footer class="entry-footer">
					<?php wp_bootstrap_starter_entry_footer(); ?>
				</footer><!-- .entry-footer -->
			</a>
		</article><!-- #post-## -->

	</div>

<?php endif; ?>

