
<div class="col-12 col-md-6 col-lg-4 col-xl-3 text-center item p-2">
	<a class="link-recipe" href="<?php the_permalink(); ?>">
		<figure>
			<div class="bg-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div><!-- /.bg-img -->
			<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
		</figure>
		<p class="title-recipe"><?php the_field('recipe-title'); ?></p>
	</a>
	<div class="product-reviews">
		<div id="pr-reviewsnippet-recipe-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
	</div>
</div><!-- /.col-12 col-md-3 -->