<div class="nav-fixed">
	<h5>Filter all recipes</h5>
	<div class="cont-options">
		<?php $search_filter_id = get_field( 'search_and_filter_id', 'option' ); ?>
		<?php echo do_shortcode( '[searchandfilter id="'.$search_filter_id.'"]' ); ?>

		<?php// echo do_shortcode( '[searchandfilter submit_label="Filter" fields="category" order_by="count" types="checkbox" hierarchical="1" operators="AND"]' ); ?>
	</div><!-- /.radio-list -->
</div><!-- /.nav-fixed -->