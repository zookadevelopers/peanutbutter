<div class="col-6 col-md-4 col-lg-3 item<?php if ( get_field('product-new') ): ?> new-product<?php endif ?>">
	<a class="link-product product-img text-center" href="<?php the_permalink(); ?>">
		<figure>
			<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
		</figure>

		<?php $product_name = get_field('product-name'); ?> 

		<p><?php

			if (strlen($product_name) > 60) {
				$product_name .= '...';
			}

			echo substr($product_name, 0, 70);
			?>
		</p>

		<small>
			<?php the_field('product-heat-indicator'); ?>
			<?php the_field('product-style'); ?>		
		</small>
	</a>
	<div class="product-reviews">
		<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
	</div>
</div>