<section class="categories-filters">
	<div class="dropdown">
		<div class="cont-button-filter dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<div class="container">
				<div class="row align-items-baseline text-center">

					<div class="col-12 offset-md-4 col-md-4 col-lg-4">
						<h2><?php echo ('Filter recipes by');?></h2>
					</div><!-- /.col-10 -->
					<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-right">
						<div class="right-buttons">
							<a href="<?php echo home_url('/recipes'); ?>" class="reset-filters">Reset Filter</a>
						</div><!-- /.right-buttons -->
					</div><!-- /.col-2 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.cont-button-filter -->
		
		<div class="main-parent-filter dropdown-menu" aria-labelledby="dropdownMenuButton">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="radio-list">
							<?php echo do_shortcode( '[searchandfilter submit_label="Filter" fields="category" order_by="count" types="checkbox" hierarchical="1" operators="OR"]' ); ?>
						</div><!-- /.radio-list -->
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.main-parent-filter -->
	</div><!-- /.dropdown -->
</section><!-- /.categories-filters -->