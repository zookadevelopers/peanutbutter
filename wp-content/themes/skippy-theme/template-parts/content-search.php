<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
?>

<div class="col-12 item-search">
	<?php if (get_the_post_thumbnail_url()): ?>
		<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
	<?php endif ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-header">
			<?php the_title( sprintf( '<p class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></p>' ); ?>
		</div><!-- .entry-header -->

		<div class="entry-summary">
			<?php if (get_the_excerpt() == false): ?>
			<p><?php 
				$description = get_field('product-description');
				$more_info = '';
				if (strlen($description) > 270) {
					$more_info = '...';
				}
                echo $description = substr($description, 0, 270) . $more_info;
            ?></p>		
			<?php endif ?>
				<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

	</article><!-- #post-## -->
	<div class="cont-button">
		<!-- <a href="<?php //echo get_permalink( $post->ID ); ?>" class="btn btn-main">View</a> -->
	</div><!-- /.cont-button -->
	
</div>
	