<?php
	if ( is_single() || is_front_page() ) {
	    $column = 'col-xl-3';
	} else {
	    $column = 'col-xl-4';
	}

global $wp;
if  (is_front_page() ): 
	$recipe_page_url = home_url( $wp->request ).'/recipes';
else:
	$recipe_page_url = home_url( $wp->request );
endif;

?>
<div class="col-6 col-md-6 col-lg-4 <?php echo $column; ?> text-center item p-2">
	<a class="link-recipe" href="<?php echo $recipe_page_url; ?>/<?php echo $post_slug; ?>">
		<figure>
			<div class="bg-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div><!-- /.bg-img -->
			<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
		</figure>
		<p class="title-recipe"><?php the_field('recipe-title'); ?></p>
	</a>
	<div class="product-reviews">
		<div id="pr-reviewsnippet-recipe-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
	</div>
</div><!-- /.col-12 col-md-3 -->