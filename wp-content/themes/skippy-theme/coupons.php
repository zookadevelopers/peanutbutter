<?php 
// Template Name: Coupons

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?>

<?php get_header(); ?>

<div class="coupons-container">
	<div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h1>Coupons, Promotions & Deals</h1>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->


	<section class="main-coupons-integration text-center">
		<h5>coupons.com integration</h5>
	</section><!-- /.main-coupons -->


	<?php
		$posts = get_field('featured-category');
		if( $posts ):
	?>
	<section class="featured-category banner-product container-fluid">
		<div class="container">
			<?php foreach( $posts as $post): ?>
				<?php setup_postdata($post); ?>
			<div class="row justify-content-center">
				<div class="col-12 col-md-8">
					<h1><?php the_field('title_category'); ?></h1>
					<p><?php the_field('category_description'); ?></p>
				</div>
			</div><!-- /.row -->
			<div class="row justify-content-center">
				<div class="col-12 col-lg-5">
					<figure class="category-img">
						<img src="<?php the_post_thumbnail_url(); ?>" class="img-fluid" width="auto" height="auto" alt="" />
					</figure>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			<?php endforeach; ?>
		</div>
	</section>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>


	<?php
		$posts = get_field('trending-products');
		if( $posts ):
	?>
	<section class="trending-products products-home">
		<div class="container">
			<div class="row mb-4">
				<div class="col-12">
					<h3>Trending products</h3>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			<div class="row items-products list-product page-products">
				<?php foreach( $posts as $post): ?>
				<?php setup_postdata($post); ?>
				<?php
					$pageId = get_field('page_id');
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
						'locale' => 'en_US',
						'merchant_group_id' => '78292',
						'merchant_id' => '146622',
						'page_id' => strval($pageId),
						'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
						]
					];
				?>
				<div class="col-6 col-md-6 col-lg-3 item<?php if ( get_field('product-new') ): ?> new-product<?php endif ?>">
					<a class="link-product product-img text-center" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="" />
						</figure>
						<p><?php the_field('product-name'); ?></p>
						<small>
							<?php the_field('product-heat-indicator'); ?>
							<?php the_field('product-style'); ?>		
						</small>
					</a>
					<div class="product-reviews">
						<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
					</div>
				</div>
				<?php endforeach; ?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.related-category -->
		<?php wp_reset_postdata(); ?>
	<?php endif; ?>	
</div><!-- /.coupons-container -->

<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php get_footer(); ?>