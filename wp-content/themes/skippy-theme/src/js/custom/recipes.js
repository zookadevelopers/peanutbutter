$(document).ready(function () {
	$('.ssm-toggle-nav').on('click', function(){
		if ($(this).hasClass('title-filters')) {
			$('.cont-mobile-menu .visible-menu, .cont-mobile-menu .mobile-logo').hide();
			$('.cont-mobile-menu .visible-filters').show();

		} else {
			$('.cont-mobile-menu .visible-menu, .cont-mobile-menu .mobile-logo').show();
			$('.cont-mobile-menu .visible-filters').hide();
		}
	});
});