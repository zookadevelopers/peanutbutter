var $ = jQuery;
var windowWidth = $(window).width();

$(window).load(function() {
	//loader
	$(".loader").fadeOut("slow");

	if (windowWidth <= 768) {
		$('[data-toggle="dropdown"]').on('click touchstart', function(){
			$('.dropdown-toggle').dropdown();
		});

		$('.cont-mobile-menu').slideAndSwipe();

		// filters options toggle function
		var mainBrands = $('.left-filters .cont-options .sf-field-category > ul > li > label');

		for (var i = 0; i < mainBrands.length; i++) {
		    mainBrands[i].addEventListener('click', function(event) {
		        event.preventDefault();
		        this.classList.toggle('active');
		        var content = this.nextElementSibling;
		        if (content.style.display === 'block') {
		            content.style.display = 'none';
		        } else {
		            content.style.display = 'block';
		        }
		    });
		}
	}

	var categoryOption = $(".nav-filters ul.parent-list label input[type='checkbox']");

	categoryOption.on('click', function(){
		console.log($(this));
		$('.main-filter-form').submit();
	});

	var coll = $('.cont-mobile-menu .nav-filters ul.parent-list > li:first-child > ul:first-child > li > label');
	var i;

	for (i = 0; i < coll.length; i++) {
	  coll[i].addEventListener('click', function(event) {
	  	event.preventDefault();
	    this.classList.toggle('active');
	    var content = this.nextElementSibling;
	    if (content.style.display === 'block') {
	      content.style.display = 'none';
	    } else {
	      content.style.display = 'block';
	    }
	  });
	}
});

$(document).ready(function () {

	/* M E N U   M O B I L E  */
    $('.site-content').on('click', '.overlay-custom', function(){
        if ($('#simple-menu i').hasClass('fa-close')) {
            $('#simple-menu').click();
        }
    });

	$('#carousel-home').owlCarousel({
		items: 1,
		nav: true,
		loop: true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		transitionStyle: "fade"
	});

	$('#carousel-video').owlCarousel({
		loop: false,
		margin: 10,
	    items:3,
		touchDrag  : true,
		mouseDrag  : true,
		navText : [
			"<img class='img-fluid' src='"+wnm_custom.template_url+"/src/img/arrow-left-black.png' />",
			"<img class='img-fluid' src='"+wnm_custom.template_url+"/src/img/arrow-right-black.png' />"
		],
		responsive:{
			0:{
				items: 1,
				stagePadding: 80,
				loop: false,
				nav: false,
				center: true
			},
			992:{
				items: 2,
				stagePadding: 100
			},
			1440:{
				items: 3,
				nav: true
			}
		}
	});

	/* Read more or less in recipe page */
    var btn_read = $('.recipe-description .read-more');
    btn_read.on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        $('.recipe-description p').toggleClass('showing');
        $this.text($this.text() == 'Read More' ? 'Read Less' : 'Read More');
    });

	if (windowWidth <= 768) {
		$('.cont-categories-filters .categories > ul > .cat-item > a').on('click', function(e){
			console.log('click');
			e.preventDefault();
		});

		$('[data-toggle="dropdown"]').on('click touchstart', function(){
			$('.dropdown-toggle').dropdown();
		});
	}

	var modalId;
	$('.button-play').on('click', function(e) {
		modalId = $(this).data( "target" );
		$(modalId+'.modal-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');

	});

	$('.modal-video, .modal-video [data-dismiss="modal"]').on('click', function(e) {
		modalId = $(this).parents().find('.modal-video.show').attr('id');
		$('#'+modalId+'.modal-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
	});


	if ($('.search-field').val() != "") {
		$('.search-form').toggleClass('active');
	}

	$('.search-submit').on('click', function(e){
		$('.search-form').toggleClass('active');
		
		if ($('.search-field').val() == "") {
			e.preventDefault();
		}
	});

	//replace ® by sup tag
	// $('body:not(.single) *:contains("®")').html(function(_, html) {
	// 	return  html.replace(/(®)/g, '<sup>®</sup>');
	// });

	//append current filter category
	var allInputs = $('.categories-filters ul.parent-list label input[type="checkbox"]:checked');
	var length = allInputs.length;
	allInputs.each(function(index, element){
		$('.search-categories').append('"'+$(this).parent().text()+'" ');
	});

	// reemplace string for newsletter placeholder input
	$('.tnp-email').attr('placeholder', 'Email...');

	// scroll to category products
	$(document).on('click', '.items-all-products a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    //contact page - show and hide section
    $('#is-product-content').hide();
	$('[class*="acceptance"]').on('click', function(){
		var contactSpecificProduct = $('[type="checkbox"][name="acceptance-840"]').attr('checked') ? true:false;
		
		if (contactSpecificProduct == true) {
			$('#is-product-content').show();
		} else {
			$('#is-product-content').hide();
		}
	});

	var productName = $('#form-select-product-name');

	productName.on('change', function(){
		if ($(this).val() == 'Product name') {
			$('.upc-normal').removeClass('d-block').addClass('d-none');
			$('.upc-required').removeClass('d-none').addClass('d-block');
			$('.upc-required input[type="number"]').attr('value', '');
		} 
		else {
			$('.upc-required').removeClass('d-block').addClass('d-none');
			$('.upc-normal').removeClass('d-none').addClass('d-block');
			$('.upc-required input[type="number"]').attr('value', 0);
		}
	});

	$('.sizes-list [data-size]').on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		var body = $('body');
		var thisParent = $this.parent();
		var thisSize = $this.data('size');
		var thisSizeText = $this.text();
		var thisPosition = $this.data('position');
		var parentList = $('.sizes-list');

		$('.btn-dropdown-sizes').text(thisSizeText);
		
		var currentActiveElements = body.find('[data-size='+thisSize+'][data-status="active"]');
		var inactiveElements = body.find('[data-size='+thisSize+'][data-status="inactive"]');
		var activeElements = body.find('[data-status="active"]');

		if (!thisParent.hasClass('active')) {
			parentList.find('li').removeClass('active');
			thisParent.addClass('active');
		}

		if (currentActiveElements.data('status') != 'active') {
			$.each( inactiveElements, function(index){
				$(this).attr('data-status', 'active');
			});

			$.each( activeElements, function(index){
				$(this).attr('data-status', 'inactive');
			});
		}
	});

	//Back to top button functionallity
    (function($, window, document){

        var backToTopButton = $('.back-top');

        function backToTop(selector, time) {
            $('html, body').animate({
                scrollTop: $(selector).offset().top -30
            }, time);
        }

        $(document).scroll(function(){
            if($(window).scrollTop() + $(window).height() == $(document).height() || $(this).scrollTop() >= 700){
                backToTopButton.fadeIn();
            }else{
                backToTopButton.fadeOut();
            }
        });

        backToTopButton.on('click', function() {
            backToTop('body', 400);
            return false;
        });

    })(jQuery, window, document);
});


//reset filters button
$('.reset-filters').on('click', function(e){
	console.log('clicked');
	e.preventDefault();
	$('.nav-filters input:checkbox').removeAttr('checked');
	window.location.href = '/recipe';
});

/* Instagram Icon with hover functionality */
$('ul.ssbp-list').append("<li class='ssbp-li--instagram item-instagram'><a href='https://www.instagram.com/skippybrand' target='blank'><img class='img-fluid' src='"+wnm_custom.template_url+"/src/img/global/instagram.svg' data-hover='"+wnm_custom.template_url+"/src/img/global/instagram-hover.png' data-original='"+wnm_custom.template_url+"/src/img/global/instagram.svg' alt='social' width='auto' height='auto' /></a></li>");
$('.item-instagram a').mouseover(function() {
	$('.item-instagram a img').attr("src", $('.item-instagram a img').data("hover"));
}).mouseout(function() {
	$('.item-instagram a img').attr("src", $('.item-instagram a img').data("original"));
})


/* R E V I E W S */

$('btn-write-review').on('click', function(e) {
	var target = $('.cont-write-review a').attr('href');
	if (target != "") {
		window.location.href = target;
	} else {
		e.preventDefault();
	}
		
});

// scrolldown to review section
$(".review").click(function(e){
	e.preventDefault();
	$("html,body").animate({scrollTop:$(".featured-recipes-home").offset().top},"slow")
});
