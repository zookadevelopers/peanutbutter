<?php 
// Template Name: Home


$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

// Products Query
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
); 

// Recipes query
$recipes = new WP_Query(
	array(
		'post_type'=>'post', 
		'post_status'=>'publish', 
		'posts_per_page'=> 4,
		'order' => 'ASC'
	)
); 
?>

<?php get_header(); ?>

<section class="banner-home container-fluid" style="background-image: url(<?php the_field('banner_image'); ?>)">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8">
			<div class="title-banner text-center">
				<img class="img-fluid" alt="SKIPPY" src="<?php the_field('title_banner'); ?>"/>
			</div><!-- /.title-banner text-center -->	
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
	<div class="text-center product-banner">
		<img src="<?php the_post_thumbnail_url(); ?>" alt="SKIPPY Product Banner" class="img-fluid">
	</div>
</section>

<div class="cont-button container">
	<div class="row">
		<div class="col-12 text-center">
			<a class="btn btn-second" href="<?php the_field('button_link'); ?>" title="<?php the_field('button_text'); ?>"><?php the_field('button_text'); ?></a>
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
</div><!-- /.cont-button -->

<section class="title">
	<h1>Be smooth like SKIPPY® peanut butter</h1>
</section><!-- /.title -->

<section class="products-home">
	<div class="container">
		<div class="row items-products">
			<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
				<div class="col-6 col-md text-center item p-2">
					<a class="link-product" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
						</figure>
						<p class=""><?php the_title(); ?></p>
					</a>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<section class="cont-recipes list-recipes">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>Explore recipes</h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row recipes justify-content-center">
			<?php while ( $recipes->have_posts() ) : $recipes->the_post(); global $post;?>
				<?php
					$pageId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );
					$post_slug = get_post_field( 'post_name', get_post() );

					$objPowerReviews[] = [
						'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
						'locale' => 'en_US',
						'merchant_group_id' => '78292',
						'merchant_id' => '146622',
						'page_id' => strval($pageId),
						'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-recipe-'.$pageId
						]
					];
				?>
			<?php require "template-parts/recipe-item.php"; ?>
			<?php endwhile; wp_reset_postdata(); ?>
		</div><!-- /.row -->
		<div class="d-none d-md-block">
			<div class="row">
				<div class="col-12">
					<a href="<?php echo get_site_url(); ?>/recipes" class="btn btn-main">See all recipes</a>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->			
		</div><!-- /.d-none d-md-block -->
	</div><!-- /.container -->
</section><!-- /.cont-recipes -->

<section class="cont-videos">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2>Click play to see smooth in action</h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->

		<?php if( have_rows('group_videos') ): ?>
		<div class="row justify-content-center videos">
			<?php $count = 1; while( have_rows('group_videos') ): the_row(); 


				$video = get_sub_field('video');
				$title = get_sub_field('video_title');
				$poster = get_sub_field('video_poster');
				$videoUrl = get_sub_field('video_url');

			?>
			<div class="col-12 col-sm-6 col-md-4 col-lg-4 cont-item">
				<a href="#" data-toggle="modal" data-target="#home-video-<?php echo $count; ?>" class="button-play">
					<div class="item">
						<figure>
							<img src="<?php echo $poster; ?>" class="img-fluid" alt="<?php echo $title; ?>" />
						</figure>
						<p><?php echo $title; ?></p>
					</div>
				</a>
			</div><!-- /.col-12 col-sm-6 col-md-4 col-lg-4 -->
			<?php $count++; endwhile; ?>
		</div>
		<?php else: ?>

			<p>No videos available</p>

		<?php endif; ?>
	</div><!-- /.container -->
</section><!-- /.cont-videos -->

<?php if( have_rows('group_videos') ): ?>
	<?php $count = 1; while( have_rows('group_videos') ): the_row(); 

		$videoUrl = get_sub_field('video_url');
	?>
		<!-- Modal -->
		<div class="modal fade modal-video" id="home-video-<?php echo $count; ?>" tabindex="-1" role="dialog" aria-labelledby="homeVideoPopup-<?php echo $count; ?>" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="cont-popup-video">

							<?php 
						
							// use preg_match to find iframe src
							preg_match('/src="(.+?)"/', $videoUrl, $matches);
							$src = $matches[1];

							// add extra params to iframe src
							$params = array(
								'version'   		 => 3,
								'enablejsapi'        => 1,
								'autohide'    => 1
							);

							$new_src = add_query_arg($params, $src);

							$videoUrl = str_replace($src, $new_src, $videoUrl);

							// add extra attributes to iframe html
							$attributes = 'frameborder="0"';

							$videoUrl = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $videoUrl);

							echo $videoUrl;
							
							?>

						</div><!-- /.cont-popup-video -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	<?php $count++; endwhile; ?>

<?php endif; ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php 
 get_footer();
