<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
global $faqLink;

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
				</div><!-- /.col-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->	

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div class="container">
			<div class="top-footer">
				<div class="row justify-content-center align-items-center text-center">
					<div class="col-4">
						<?php if (is_page_template('page-faq.php')): ?>
						<a id="faq-action" data-toggle="collapse" data-target="#collapseFaq" aria-expanded="false" aria-controls="collapseFaq" data-action="1">	
						<?php else: ?>
						<a href="<?php echo site_url(); ?>/faq">
						<?php endif ?>
							<div class="info-content">
								<figure class="global-icon">
									<img class="img-fluid" src="<?php echo $upload_theme ?>global/icon-faq.svg" alt="global" width="auto" height="auto" />
								</figure>
								<p>FAQ</p>
							</div><!-- /.info-content -->
						</a>
					</div><!-- /.col-3 -->
					<div class="col-4">
						<a href="<?php echo esc_url( home_url( '/' )); ?>#">
							<div class="info-content">
								<figure class="global-icon">
									<img class="img-fluid" src="<?php echo $upload_theme ?>global/icon-where.svg" alt="global" width="auto" height="auto" />
								</figure>
								<p>Where to buy</p>
							</div><!-- /.info-content -->
						</a>
					</div><!-- /.col-3 -->
					<div class="col-4">
						<a href="<?php echo site_url(); ?>/coupons">
							<div class="info-content">
								<figure class="global-icon">
									<img class="img-fluid" src="<?php echo $upload_theme ?>global/icon-offer.svg" alt="global" width="auto" height="auto" />
								</figure>
								<p>Special offers</p>
							</div><!-- /.info-content -->
						</a>
					</div><!-- /.col-3 -->
				</div><!-- /.row -->
			</div><!-- /.top-footer -->
		</div><!-- /.container -->

		<?php get_template_part( 'footer-widget' ); ?>

		<div class="container pt-3 pb-3">
			<div class="row">
	            <div class="site-info col-12 col-lg-12 text-center">
	            	<div class="d-block d-sm-flex align-items-center flex-column flex-sm-row">
		                <?php dynamic_sidebar( 'footer-copyright' ); ?>
	            	</div>
	            </div><!-- close .site-info -->
            </div><!-- /.row -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div>
</div><!-- #page -->
<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
<script src="http://labs.rampinteractive.co.uk/touchSwipe/jquery.touchSwipe.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>