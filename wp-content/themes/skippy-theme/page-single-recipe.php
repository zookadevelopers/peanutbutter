<?php 
// Template Name: Single Recipe

$slug = $wp_query->query_vars['slug'];
$args = array(
	'name'        => $slug,
	'post_type'   => 'post',
	'post_status' => 'publish',
	'numberposts' => 1
);
//echo "<h1>".$slug."</h1>";

$loop = new WP_Query( $args );


$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

$pageId = get_the_ID();
$name = get_field('recipe-title');;
$imgUrl = get_the_post_thumbnail_url();
$description = get_field('recipe-description');
$postUrl = get_post_permalink( $pageId );

?> 

<?php get_header(); ?>

<?php if( $loop->have_posts() ) : 
while ( $loop->have_posts() ) : $loop->the_post(); global $post;?>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>


<?php if(get_field('banner_image') ): ?>
	<section class="recipe-banner" style="background-image: url('<?php the_field('banner_image'); ?>');">
<?php else: ?>
	<section class="recipe-banner" style="background-image: url('<?php the_field('recipe_featured_image'); ?>');">
<?php endif; ?>

<?php if (get_field('recipe-video')): ?>
	<div class="cont-recipe-video">
		<?php the_field('recipe-video') ?>
		<div class="button-play" id="play-button" data-toggle="modal" data-target="#recipeVideo"></div><!-- /.button-play -->
	</div><!-- /.cont-recipe-video -->
<?php endif ?>
</section><!-- /.recipe-banner -->

<div class="recipe-main">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Home</a></li>
						<li class="breadcrumb-item"><a href="<?php echo home_url('/recipes'); ?>">All recipes</a></li>
						<li class="breadcrumb-item active" aria-current="page"><?php the_field('recipe-title'); ?></li>
					</ol>
				</nav>
			</div><!-- /.col-12 -->

			<div class="col-12 col-lg-12">
			
				<h1><?php the_field('recipe-title'); ?></h1>
				<div class="recipe-reviews">
					<div class="product-reviews">
						<div id="pr-reviewsnippet-top-<?php the_field('page_id') ?>" class="stars-snippet-top category-page"></div>
					</div>
				</div><!-- /.recipe-reviews -->


				<div class="recipe-description col-lg-10 mx-auto">
                    <div class="d-none d-md-block">
                        <?php
                            $description = get_field('recipe-description');
                        ?>    
                        <p<?php if (strlen($description) < 100) : ?> class="showing"<?php endif; ?>><?php the_field('recipe-description'); ?></p>
                        <?php if (strlen($description) > 100) : ?>
                        <div class="cont-show-more">
                            <a href="#" class="btn btn-link read-more">Read More</a>
                        </div>
                        <?php endif; ?>
                    </div><!-- /.d-none -->

                    <div class="d-block d-md-none">
                        <p><?php the_field('recipe-description'); ?></p>
                        <div class="cont-show-more">
                            <a href="#" class="btn btn-link read-more">Read More</a>
                        </div>
                    </div><!-- /.d-block -->
                </div><!-- /.recipe-description -->
			
			<div class="col-12 p-0">
				<div class="border-recipe">
					<div class="col-12 col-lg-12 px-4">
						<div class="recipe-information">
							<?php if( get_field('recipe-servings') ): ?>
							<div class="recipe-servings information-item">
								<label>Servings</label>
								<span><?php the_field('recipe-servings'); ?></span>
							</div><!-- /.recipe-servings -->
							<?php endif; ?>

							<?php if( get_field('recipe-calories-servings') ): ?>
							<div class="recipe-calories information-item">
								<label>Calories/serving</label>
								<span><?php the_field('recipe-calories-servings'); ?></span>
							</div><!-- /.recipe-calories -->
							<?php endif; ?>

							<?php if( get_field('recipe-prep-time') ): ?>
							<div class="recipe-prep-time information-item">
								<label>Prep Time</label>
								<span><?php the_field('recipe-prep-time'); ?></span>
							</div><!-- /.recipe-prep-time -->
							<?php endif; ?>
							
							<?php if( get_field('recipe-cook-time') ): ?>
							<div class="recipe-cook-time information-item">
								<label>Cook Time</label>
								<span><?php the_field('recipe-cook-time'); ?></span>
							</div><!-- /.recipe-cook-time -->
							<?php endif; ?>

							<?php if( get_field('recipe-total-time') ): ?>
							<div class="recipe-total-time information-item">
								<label>Total Time</label>
								<span><?php the_field('recipe-total-time'); ?></span>
							</div><!-- /.recipe-total-time -->
							<?php endif; ?>

						</div><!-- /.recipe-information -->
					</div><!-- /.border-recipe -->
				</div>
			</div><!-- /.col-12 -->

			<div class="col-12 col-lg-10 mx-auto">
				<div class="cont-columns">
					<div class="row">
						<div class="col-lg-4 col-12">
							<div class="column col-left col-ingredients">
								<h4>Ingredients</h4>
								<?php the_field('recipe-ingredients'); ?>
							</div><!-- /.col-left col-ingredients -->
						</div><!-- /.col-lg-5 col-12 -->
						<div class="col-lg-7 offset-lg-1 col-12 px-lg-0">
							<div class="column col-right col-directions">
								<h4>Directions</h4>
								<?php the_field('recipe-directions'); ?>
							</div><!-- /.col-right col-directions -->
						</div><!-- /.col-lg-7 col-12 -->
					</div><!-- /.row -->
				</div><!-- /.cont-columns -->
			</div>
				
			<div class="col-12 p-0 px-lg-3">
				<div class="border-recipe recipe-content">
					<div class="col-12 offset-lg-1 col-lg-10">
						<div class="recipe-share">
							<div class="row align-items-center">
								<div class="col-9 col-lg-5">
									<?php echo do_shortcode('[ssba-buttons]'); ?>
								</div><!-- /.col-12 -->
								<div class="col col-lg-6 mar-lef-rec p-0 px-lg-3">
									<span class="by-brand">Recipe by SKIPPY®</span>
									<span class="date"><?php the_field('recipe-date'); ?></span>
								</div><!-- /.col-lg-7 col-12 -->
							</div><!-- /.row -->
						</div><!-- /.recipe-share -->
					</div>
				</div>
			</div>
			
			<div class="col-12 offset-lg-1 col-lg-10">
				<?php if (get_field('recipe-video')): ?>
					<!-- Modal -->
					<div class="modal fade recipe-video" id="recipeVideo" tabindex="-1" role="dialog" aria-labelledby="recipeVideoTitle" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="cont-popup-recipe-video">
										<?php the_field('recipe-video') ?>
									</div><!-- /.cont-recipe-video -->
							
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>
					</div>
				<?php endif ?>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.recipe-main -->

<?php if ( get_field('page_id') ) { ?>
	<section class="featured-recipes-home">
		<div class="title">
			<div class="container">
				<div class="row">
					<div class="col offset-lg-1">
						<h2>Reviews</h2>
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.title -->
		<div class="content">
			<div class="container">
				<div class="row columns">
					<div class="col-md-4 col-12 offset-lg-1">
						<div id="pr-reviewsnippet-<?php the_field('page_id') ?>" class="stars-snippet"></div>
						<div id="pr-reviewhistogram" class="p-w-r">
							<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
						</div>
						<div class="cont-write-review"></div><!-- /.cont-write-review -->

					</div><!-- /.col-5 -->
					<div class="col-md-7 col-12">
						<div id="pr-reviewdisplay-<?php the_field('page_id') ?>" class="cont-review-display"></div>
					</div><!-- /.col-7 -->
				</div><!-- /.row -->
			</div>
		</div>
	</section>
<?php } ?>

<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
<script type="text/javascript" charset="utf-8">

	var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	var urlSite = '<?= get_site_url(); ?>';

	POWERREVIEWS.display.render(
		[
			{
				api_key: '7783bde7-0813-4233-8474-9c2889af0d40',
				locale: 'en_US',
				merchant_group_id: '78292',
				merchant_id: '146622',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: urlSite+'/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: templateUrl+'/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
				}
			},
			{
				api_key: '7783bde7-0813-4233-8474-9c2889af0d40',
				locale: 'en_US',
				merchant_group_id: '78292',
				merchant_id: '146622',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: urlSite+'/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: templateUrl+'/inc/assets/css/custom/reviews.css',
				on_back_to_top_click: function() {
					jQuery('html, body').animate({
						scrollTop: jQuery(".featured-recipes-home").offset().top - 40
					}, 1000);
				},
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.stars-snippet .pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn btn-main');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
				}
			}
		]
	);
</script>