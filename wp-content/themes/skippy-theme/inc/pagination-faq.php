<?php
// Hook for no logged users
add_action('wp_ajax_nopriv_faq_product_pagination', 'faq_product_pagination');
// Hook for logged in users
add_action('wp_ajax_faq_product_pagination', 'faq_product_pagination');

function faq_product_pagination(){

	// Getting the FAQ Page ID used to get the custom fields of the FAQ page.
	$faq_page_id = get_field('faq_page_id', 'option');

	$page_number = isset( $_POST['page_number'] ) ? (int) $_POST['page_number'] : 1;
	$tab 	= isset( $_POST['tab'] ) ? sanitize_text_field ( $_POST['tab'] ) : false;
	
	//$paged3 = isset( $_POST['paged3'] ) ? (int) $_POST['paged3'] : 1;

	if ( (!$page_number) || (!$tab) ) :
		// Error Response
		wp_send_json( array('message' => __('A field is empty', 'wpduf') ) );
	else:
	// Succes Response
	// Variables
		$row_prod			= 0;
		$row_recipe			= 0;
		$row_ingre 			= 0;

		$items_per_page		= 6;

		$faq_prod 			= get_field('content-products', $faq_page_id[0]);
		$faq_recipe			= get_field('content-recipes', $faq_page_id[0]);
		$faq_ingre 			= get_field('content-ingredients', $faq_page_id[0]);

		$total_prod 		= count( $faq_prod );
		$total_recipe 		= count( $faq_recipe );
		$total_ingre 		= count( $faq_ingre );
		
		


		$min_prod			= ( ( $page_number * $items_per_page ) - $items_per_page ) + 1;
		$min_recipe			= ( ( $page_number * $items_per_page ) - $items_per_page ) + 1;
		$min_ingre 			= ( ( $page_number * $items_per_page ) - $items_per_page ) + 1;

		$max_prod 				= ( $min_prod + $items_per_page ) - 1;
		$max_recipe				= ( $min_recipe + $items_per_page ) - 1;
		$max_ingre 				= ( $min_ingre + $items_per_page ) - 1;

		switch ($tab) {
			case 'products':

				$total_btn = ceil( $total_prod / $items_per_page );
				
				if( have_rows('content-products', $faq_page_id[0]) ):

					$count = 1; while( have_rows('content-products', $faq_page_id[0]) ): the_row(); 

					$row_prod++;
					if($row_prod < $min_prod) { continue; }
					if($row_prod > $max_prod) { break; }

					$question = get_sub_field('product-question');
					$answer = get_sub_field('product-answer');

					$faq_items[] = array(
						'question' => $question,
						'answer' => $answer
					);

					$count++; endwhile;
				endif;

				break;
			
			case 'recipes':

				$total_btn		= ceil( $total_recipe / $items_per_page );
				
				if( have_rows('content-recipes', $faq_page_id[0]) ):

					$count = 1; while( have_rows('content-recipes', $faq_page_id[0]) ): the_row(); 

					$row_recipe++;
				    if($row_recipe < $min_recipe) { continue; }
				    if($row_recipe > $max_recipe) { break; }

					$question = get_sub_field('recipe-question');
					$answer = get_sub_field('recipe-answer');

					$faq_items[] = array(
						'question' => $question,
						'answer' => $answer
					);

					$count++; endwhile;
				endif;
				
				break;
			
			case 'ingredients':

				$total_btn = ceil( $total_ingre / $items_per_page );
				
				if( have_rows('content-ingredients', $faq_page_id[0]) ):

					$count = 1; while( have_rows('content-ingredients', $faq_page_id[0]) ): the_row(); 

					$row_ingre++;
					if($row_ingre < $min_ingre) { continue; }
					if($row_ingre > $max_ingre) { break; }

					$question = get_sub_field('ingredient-question');
					$answer = get_sub_field('ingredient-answer');

					$faq_items[] = array(
						'question' => $question,
						'answer' => $answer
					);

					$count++; endwhile;
				endif;
				
				break;
			

		}

		$pag_items_args = array(
            'current' => $page_number,
            'total' => $total_btn,
            'prev_text' => __('<i class="fas fa-angle-left" data-info="prev"></i>'),
			'next_text' => __('<i class="fas fa-angle-right" data-info="next"></i>'),
        );
        $pag_items_btn = paginate_links( $pag_items_args );
        $pag_items_btn = str_replace("'", '"', $pag_items_btn);

		wp_send_json( array(
			'tab' => $tab,
			'faq_items' => $faq_items,
			'pag_items_btn' => $pag_items_btn
		) );
	endif;
}
?>