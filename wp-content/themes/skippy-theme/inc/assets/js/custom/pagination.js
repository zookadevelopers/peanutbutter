var $ = jQuery;

//Add data action next button
/*var currentPage = $('[data-action="faq_product_pagination"] .current').text();
var nextPage = parseInt(currentPage) + 1;
var prevPage = parseInt(currentPage) - 1;

var btnPrev = $('[data-action="faq_product_pagination"] .prev').attr('id', prevPage);
var btnNext = $('[data-action="faq_product_pagination"] .next').attr('id', nextPage);
*/
// pagination faq section
var globalFaq = $('#faq-action').data('action');
var urlAjax = $('.pagination').data('ajax-url');
var product = $('.pagination').data('action');

//first load page
/*$('#faq-action').on('click', function(){
	
	//Ajax Request
	$.ajax({
		method : "post",
		url : urlAjax,
		data : {
			action:product,
			paged1: globalFaq
		},
		success: function(response) {

			var faq = response.faq_products;
			$('#container-questions').html("");
			var print = "";
			var count = 0;
	
			faq.forEach(function(element) {
				print += '<div class="card-header-custom" id="fpo-'+count+'"><button class="btn btn-link" data-toggle="collapse" data-target="#collapse-fpo-'+count+'" aria-expanded="false" aria-controls="collapse-fpo-'+count+'">'+element.question+'</button></div><div id="collapse-fpo-'+count+'" class="collapse" aria-labelledby="fpo-'+count+'" data-parent="#fpo'+count+'"><div class="card-body">'+element.answer+'</div></div>';

				count++;
			});

			$('#container-questions').html(print);
		}
	});
	//End Ajax
});
*/
//change page 

function ajax_faq_pagination () { 
	$('.page-numbers').on('click', function(e){
		e.preventDefault();

		//$(' span.page-numbers.current').parent().hasClass('prod');

		//get number page
		var page_number = $(this).text();
		parent = $(this).parent().data('tab');

		if (parent == 'products') {
			
			if (page_number.length == 0) {
				
				btn_page_direction = $(this).children().data('info');

				if (btn_page_direction == 'prev') {
					btn_page_direction = -1;
				} else {
					btn_page_direction = 1;
				}

				page_number = parseInt( $('.pagination.products span.page-numbers.current').text() );
				page_number = page_number + (btn_page_direction);

			}

			tab = 'products';

		} else if (parent == 'recipes') {

			if (page_number.length == 0) {
				btn_page_direction = $(this).children().data('info');

				if (btn_page_direction == 'prev') {
					btn_page_direction = -1;
				} else {
					btn_page_direction = 1;
				}

				page_number = parseInt( $('.pagination.recipes span.page-numbers.current').text() );
				page_number = page_number + (btn_page_direction);
				
			}

			tab = 'recipes';

		} else if (parent == 'ingredients') {

			if (page_number.length == 0) {
				btn_page_direction = $(this).children().data('info');

				if (btn_page_direction == 'prev') {
					btn_page_direction = -1;
				} else {
					btn_page_direction = 1;
				}

				page_number = parseInt( $('.pagination.ingredients span.page-numbers.current').text() );
				page_number = page_number + (btn_page_direction);
				
			}

			tab = 'ingredients';

		}


		console.log(page_number+' result');
		console.log(tab);

		//Ajax Request
		$.ajax({
			method : "post",
			url : urlAjax,
			data : {
				action:product,
				page_number: page_number,
				tab: tab
			},
			beforeSend: function(){
				console.log('beforesend, ajax');

			},
			error: function (jqXHR, exception) {
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Not connect.\n Verify Network.';
				} else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				} else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				} else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				} else if (exception === 'timeout') {
					msg = 'Time out error.';
				} else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				} else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
			}, 
			success: function(response) {
				console.log(response);
				var tab = response.tab;
				var faq = response.faq_items;
				var pagination_btn = response.pag_items_btn
				
				$('#'+tab+'-questions').html("");
				$('.pagination.'+tab).html("");
				$('.pagination.'+tab).html(pagination_btn);
				var print = "";
				var count = 0;

				if (tab == 'products') {
					items_counter = 'fpo';

				} else if (tab == 'recipes') {
					items_counter = 'fqrecipes';

				} else if (tab == 'ingredients') {
					items_counter = 'fqingredients';
				}
		
				faq.forEach(function(element) {
					print += '<div class="card-header-custom" id="'+items_counter+'-'+count+'"><button class="btn btn-link" data-toggle="collapse" data-target="#collapse-'+items_counter+'-'+count+'" aria-expanded="false" aria-controls="collapse-'+items_counter+'-'+count+'">'+element.question+'</button></div><div id="collapse-'+items_counter+'-'+count+'" class="collapse" aria-labelledby="'+items_counter+'-'+count+'" data-parent="#'+items_counter+''+count+'"><div class="card-body">'+element.answer+'</div></div>';

					count++;
				});

				$('#'+tab+'-questions').html(print);

				ajax_faq_pagination();
			}
		});
		//End Ajax

		//active current page
	/*	if ( $('.page-numbers').hasClass('current') ) {
			$('.page-numbers').removeClass('current');
			$(this).addClass('current');
		}

		//prev-next pagination
		if (page != 1 ){
			$('.pagination').prepend('<span class="prev"><i class="fas fa-angle-left"></i></span>');
		} else {
			$('.prev').remove();
			$('.pagination').append('<span class="next"><i class="fas fa-angle-right"></i></span>');
		}*/
	});

}

ajax_faq_pagination();
