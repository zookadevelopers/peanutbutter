<?php 
	$upload_dir = wp_upload_dir();
	$upload_theme = get_template_directory_uri().'/inc/assets/img/';
	$post = get_post();

	$customPostType = get_post_type_object($post->post_type);

	$categories = get_the_category( $post->ID );
	$cateSize = sizeof($categories);
?> 

<?php get_header(); ?>

<section class="single-product">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-10">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo home_url('/'); ?>">Home</a></li>
						<li class="breadcrumb-item"><a href="<?php echo home_url('/products'); ?>">All Products</a></li>
						<li class="breadcrumb-item"><a href="<?php echo home_url('/products/'.$customPostType->query_var); ?>"><?php echo $customPostType->label ?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><?php the_field('product-name'); ?></li>
					</ol>
				</nav>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row info-product">
			<div class="col-12 col-md-12 col-lg-5">
				<div class="product-big-image <?php if ( get_field('product-new') ): ?>new-product<?php endif ?>">

					<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_image = get_sub_field('product_group_image');
							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);
							$product_nutritional = get_sub_field('product_group_nutritional_image');
							?>
							
							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
								<figcaption class="product-upc">
									<?php echo $product_size['product-upc']; ?>
								</figcaption>
							</figure>
						<?php
							$count++;
							endwhile; wp_reset_postdata();
						?>
				</div>
				<?php else: ?>
					<div class="product-big-image">
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-12 col-md-12 col-lg-7">
				<h1 class="title-single-pro"><?php the_field('product-name'); ?><small><?php the_field('product-heat-indicator'); ?><?php the_field('product-style'); ?></small></h1>

				<div class="product-reviews">
					<div id="pr-reviewsnippet-top-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
				</div>
				
				<div class="description"><?php the_field('product-description'); ?></div>

				<div class="sizes">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<div class="sizes-list dropdown">
							<a class="btn-dropdown-sizes dropdown-toggle" href="#" role="button" id="dropdownSizes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Select</a>

							<div class="dropdown-menu" aria-labelledby="dropdownSizes">
								<?php
									$count = 1;
									while( have_rows('product_group') ): the_row(); 

									$product_size = get_sub_field('product_group_size');
									$product_size_qty = str_replace('.', '', $product_size['size_qty']);

								?>
								<a class="dropdown-item" href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a>

								<?php
									$count++;
									endwhile; wp_reset_postdata();
								?>
							</div>
						</div>
					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="product-cont-buttons">
					<div class="row align-items-center">
						<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>
						<div class="cta-info" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
							
							<?php if ( $product_size['product_buy_online'] != "" ) : ?>
							<a href="<?php echo $product_size['product_buy_online']; ?>" target="_blank">Buy now</a>
							<?php endif; ?>

							<?php if ( $product_size['product_find_store'] != "" ) : ?>
							<a href="<?php echo $product_size['product_find_store']; ?>" target="_blank">Find store</a>
							<?php endif; ?>
						</div><!-- /.cta-info -->
							<?php
								$count++;
								endwhile; wp_reset_postdata();
							?>
						<div class="write-review col">
							<a href="#" class="btn btn-main btn-write-review">Write a Review</a>
						</div><!-- /.write-review -->
						<?php endif; ?>
					</div><!-- /.row -->
				</div><!-- /.cont-write-review -->

				<div class="recipe-share product-share">
					<?php echo do_shortcode('[ssba-buttons]'); ?>
				</div>
			</div>
		</div>
	</div><!-- /.container -->
	<div class="container-fluid">
		<div class="row cards-product">
			<div class="col-12 col-md-6 p-0">
				<div class="card left">
					<div class="card-header">
						<p>Ingredients & Allergens</p>
					</div>
					<div class="card-body">
						<div class="card-text"><?php the_field('product-ingredients'); ?></div>
						<a href="#" class="img-smartlabel">
							<figure>
								<img class="img-fluid" src="<?php echo $upload_theme ?>smartlabel-singlepage.png" alt="Smartlabel" />
							</figure>
						</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6 p-0">
				<div class="card right">
					<div class="card-header">
						<p>Nutritional information</p>
					</div>
					<div class="card-body">
						<?php if( have_rows('product_group') ): ?>

							<?php
								$count = 1;
								while( have_rows('product_group') ): the_row(); 

								$product_size = get_sub_field('product_group_size');
								$product_size_qty = str_replace('.', '', $product_size['size_qty']);
								$product_nutritional = get_sub_field('product_group_nutritional_image');

							?>

							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_nutritional; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
							</figure>

							

							<?php
								$count++;
								endwhile; wp_reset_postdata();
							?>

						<?php endif; ?>

					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<?php if ( get_field('page_id') ) { ?>
	<section class="featured-recipes-home">
		<div class="title">
			<div class="container">
				<div class="row">
					<div class="col-11 offset-md-1">
						<h2>Reviews</h2>
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.title -->
		<div class="content">
			<div class="container">
				<div class="row columns">
					<div class="col-md-4 col-12 offset-lg-1">
						<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
						<div id="pr-reviewhistogram" class="p-w-r">
							<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
						</div>
						<div class="cont-write-review"></div><!-- /.cont-write-review -->

					</div><!-- /.col-5 -->
					<div class="col-md-6 col-12">
						<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
					</div><!-- /.col-7 -->
				</div><!-- /.row -->
			</div>
		</div>
	</section>
<?php } ?>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">

	var templateUrl = '<?= get_bloginfo("template_url"); ?>';
	var urlSite = '<?= get_site_url(); ?>';

	POWERREVIEWS.display.render(
		[
			{
				api_key: '7783bde7-0813-4233-8474-9c2889af0d40',
				locale: 'en_US',
				merchant_group_id: '78292',
				merchant_id: '146622',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: urlSite+'/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: templateUrl+'/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
				}
			},
			{
				api_key: '7783bde7-0813-4233-8474-9c2889af0d40',
				locale: 'en_US',
				merchant_group_id: '78292',
				merchant_id: '146622',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: urlSite+'/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: templateUrl+'/inc/assets/css/custom/reviews.css',
				on_back_to_top_click: function() {
					jQuery('html, body').animate({
						scrollTop: jQuery(".featured-recipes-home").offset().top - 40
					}, 1000);
				},
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn btn-main');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
				}
			}
		]
	);
</script>

<?php get_footer(); ?>

