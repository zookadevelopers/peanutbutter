<?php 

// Template Name: FAQs

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$url_site = get_site_url();
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="faq-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 p-0">
				<h1>How can we help you?</h1>
			</div>
		</div>
	</div>

	<div id="accordionMain" class="faq-product">
		<div class="container">
			<div class="faq-top-icons">
				<div class="row justify-content-center">
					<div class="col-5 col-xl-3 p-1 text-md-right">
						<div id="headingOne">
						  <div class="mb-0">
							<div id="faq-action" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFaq" aria-expanded="true" aria-controls="collapseFaq" data-action="1">
								<figure class="faq">
								</figure>
								<h4>FAQ</h4>
							</div>
						  </div>
						</div>
					</div><!-- /.col -->
					<div class="col-5 col-xl-3 p-1 text-md-left">
						<div id="headingTwo">
						  <div class="mb-0">
							<div id="feedback-action" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFeedback" aria-expanded="false" aria-controls="collapseFeedback">
								<figure class="product-feedback">
								</figure>
								<h4>Product feedback</h4>
							</div>
						  </div>
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.faq-top-icons -->
		</div><!-- /.container -->
		
		<div class="faqs-info">
			<?php get_template_part( 'page-faq-questions' ); ?>
		</div><!-- /.faqs-info -->
	</div><!-- /#accordion.faq-product -->
	
	<div id="accordion-contact" class="contact-info">
		<div class="container">
			<div id="heading-contact">
				<div class="contact-btn" data-toggle="collapse" data-target="#collapse-contact" aria-expanded="true" aria-controls="collapse-contact">
					<h4>Still can’t find what you looking for? <span>Contact us </span></h4>
				</div>
			</div>
		</div><!-- /.container -->

		<div id="collapse-contact" class="collapse-contact collapse" aria-labelledby="heading-contact" data-parent="#accordion-contact">
			<div class="container">
				<div class="card-body">
					<div class="form-custom">
						<h4>Contact us</h4>
						<div class="row justify-content-center">
							<div class="col-lg-7 col-12">
								<div class="form-info">
									<?php the_field('contact_form') ?>
								</div><!-- /.form-info -->
							</div><!-- /.col-lg-7 col-12 -->
						</div><!-- /.row -->
					</div><!-- /.form-custom -->
				</div>
				<div class="row text-left justify-content-center">
					<div class="col-12 col-lg-7">
						<div class="support-call d-none d-md-block">
							<div class="container">
								<div class="row">
									<div class="col-12 px-4">
										<p>For customer support call:</p> 
										<?php the_field('support-call'); ?>
									</div><!-- /.col-12 -->
								</div><!-- /.row -->
							</div><!-- /.container -->
						</div><!-- /.support-call -->
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div>
	</div><!-- /#accordion-contact.contact-info -->

</section> 

<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
