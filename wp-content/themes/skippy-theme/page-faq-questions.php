<?php

$paged1 = isset( $_GET['paged1'] ) ? (int) $_GET['paged1'] : 1;
$paged2 = isset( $_GET['paged2'] ) ? (int) $_GET['paged2'] : 1;
$paged3 = isset( $_GET['paged3'] ) ? (int) $_GET['paged3'] : 1;

	// Variables
	$row_prod			= 0;
	$row_recipe			= 0;
	$row_ingre 			= 0;

	$items_per_page		= 6;

	$faq_prod 			= get_field('content-products');
	$faq_recipe			= get_field('content-recipes');
	$faq_ingre 			= get_field('content-ingredients');


	$total_prod 		= count( $faq_prod );
	$total_recipe 		= count( $faq_recipe );
	$total_ingre 		= count( $faq_ingre );

	$pages_prod			= ceil( $total_prod / $items_per_page );
	$pages_recipe		= ceil( $total_recipe / $items_per_page );
	$pages_ingre		= ceil( $total_ingre / $items_per_page );


	$min_prod			= ( ( $paged1 * $items_per_page ) - $items_per_page ) + 1;
	$min_recipe			= ( ( $paged2 * $items_per_page ) - $items_per_page ) + 1;
	$min_ingre 			= ( ( $paged3 * $items_per_page ) - $items_per_page ) + 1;

	$max_prod 			= ( $min_prod + $items_per_page ) - 1;
	$max_recipe			= ( $min_recipe + $items_per_page ) - 1;
	$max_ingre 			= ( $min_ingre + $items_per_page ) - 1;


?>

<div class="container">
    <div id="collapseFaq" class="collapse show" aria-labelledby="headingFaq" data-parent="#accordionMain">
    	<div class="card-body">
        	<h4 class="text-center">Questions about SKIPPY<sup>®</sup> Products</h4>
	        <div id="accordionTabs">
	        	<ul class="nav nav-tabs nav-custom">
	        		<li class="nav-item">
	        			<div id="headingTabProduct">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapseTabProduct" aria-expanded="false" aria-controls="collapseTabProduct">Products</button>
						</div>
	        		</li>
	        		<li class="nav-item">
	        			<div id="headingTabRecipes">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTabRecipes" aria-expanded="false" aria-controls="collapseTabRecipes">Recipes</button>
						</div>
	        		</li>
	        		<li class="nav-item">
	        			<div id="headingTabIngredients">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTabIngredients" aria-expanded="false" aria-controls="collapseTabIngredients">Ingredients</button>
						</div>
	        		</li>
	        	</ul>

				<div id="collapseTabProduct" class="collapse-questions collapse show" aria-labelledby="headingTabProduct" data-parent="#accordionTabs">
					<?php if( have_rows('content-products') ): ?>
					<div id="products-questions">

						<?php $count = 1; while( have_rows('content-products') ): the_row(); 

							$row_prod++;
						    if($row_prod < $min_prod) { continue; }
						    if($row_prod > $max_prod) { break; }

							// vars
							$question = get_sub_field('product-question');
							$answer = get_sub_field('product-answer');
							?>

						<div class="card-header-custom" id="fpo-<?php echo $count; ?>">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-fpo-<?php echo $count; ?>" aria-expanded="false" aria-controls="collapse-fpo-<?php echo $count; ?>">
								<?php echo $question; ?>
							</button>
						</div>
						<div id="collapse-fpo-<?php echo $count; ?>" class="collapse" aria-labelledby="fpo-<?php echo $count; ?>" data-parent="#fpo<?php echo $count; ?>">
							<div class="card-body"><?php echo $answer; ?></div>
						</div>

						<?php $count++; endwhile; ?>
					</div>

					<div class="pagination products" data-tab="products" data-ajax-url="<?php echo esc_url( admin_url('admin-ajax.php') ); ?>" data-action="faq_product_pagination">
						<?php 
						echo paginate_links( array(
							'current' => $paged1,
							'total' => $pages_prod,
							'prev_text' => __('<i class="fas fa-angle-left" data-info="prev"></i>'),
				            'next_text' => __('<i class="fas fa-angle-right" data-info="next"></i>'),
						) );
						?>
					</div><!-- /.pagination -->
					<?php endif; ?>
				</div>

				<div id="collapseTabRecipes" class="collapse-questions collapse" aria-labelledby="headingTabRecipes" data-parent="#accordionTabs">
					<?php if( have_rows('content-recipes') ): ?>
					<div id="recipes-questions">
					<?php $count = 1; while( have_rows('content-recipes') ): the_row(); 

						$row_recipe++;
					    if($row_recipe < $min_recipe) { continue; }
					    if($row_recipe > $max_recipe) { break; }

						// vars
						$question = get_sub_field('recipe-question');
						$answer = get_sub_field('recipe-answer');
						?>

					
						<div class="card-header-custom" id="fqrecipes-<?php echo $count; ?>">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-fqrecipes-<?php echo $count; ?>" aria-expanded="false" aria-controls="collapse-fqrecipes-<?php echo $count; ?>">
								<?php echo $question; ?>
							</button>
						</div>
						<div id="collapse-fqrecipes-<?php echo $count; ?>" class="collapse" aria-labelledby="fqrecipes-<?php echo $count; ?>" data-parent="#fqrecipes<?php echo $count; ?>">
							<div class="card-body"><?php echo $answer; ?></div>
						</div>

					<?php $count++; endwhile; ?>
					</div>

					<div class="pagination recipes" data-tab="recipes">
						<?php
							$pag_args2 = array(
				                'current' => $paged2,
								'total' => $pages_recipe,
				                'prev_text' => __('<i class="fas fa-angle-left" data-info="prev"></i>'),
								'next_text' => __('<i class="fas fa-angle-right" data-info="next"></i>'),
				            );
						?>
					</div><!-- /.pagination -->

					<?php endif; ?>
				</div>

				<div id="collapseTabIngredients" class="collapse-questions collapse" aria-labelledby="headingTabIngredients" data-parent="#accordionTabs">
					<?php if( have_rows('content-ingredients') ): ?>
					<div id="ingredients-questions">

					<?php $count = 1; while( have_rows('content-ingredients') ): the_row(); 

						$row_ingre++;
					    if($row_ingre < $min_ingre) { continue; }
					    if($row_ingre > $max_ingre) { break; }

						// vars
						$question = get_sub_field('ingredient-question');
						$answer = get_sub_field('ingredient-answer');
						?>

						<div class="card-header-custom" id="fqingredients-<?php echo $count; ?>">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapse-fqingredients-<?php echo $count; ?>" aria-expanded="false" aria-controls="collapse-fqingredients-<?php echo $count; ?>">
								<?php echo $question; ?>
							</button>
						</div>
						<div id="collapse-fqingredients-<?php echo $count; ?>" class="collapse" aria-labelledby="fqingredients-<?php echo $count; ?>" data-parent="#fqingredients<?php echo $count; ?>">
							<div class="card-body"><?php echo $answer; ?></div>
						</div>

					<?php $count++; endwhile; ?>
					</div>

					<div class="pagination ingredients" data-tab="ingredients">
						<?php
							$pag_args3 = array(
				                'current' => $paged3,
				                'total' => $pages_ingre,
				                'prev_text' => __('<i class="fas fa-angle-left" data-info="prev"></i>'),
								'next_text' => __('<i class="fas fa-angle-right" data-info="next"></i>'),
				            );
				            echo paginate_links( $pag_args3 );
						?>
					</div><!-- /.pagination -->

					<?php endif; ?>
				</div>
			</div>
		</div>
    </div>
    
    <div id="collapseFeedback" class="collapse collapse-feedback-custom" aria-labelledby="headingFeedback" data-parent="#accordionMain">
    	<div class="container">
			<div class="card-body">
				<div class="form-custom">
					<h4>Product feedback</h4>
					<div class="row justify-content-center">
						<div class="col-lg-7 col-12">
							<div class="form-info">
								<?php the_field('product-feedback-form'); ?>
							</div><!-- /.form-info -->
						</div><!-- /.col-lg-7 col-12 -->
					</div><!-- /.row -->
				</div><!-- /.form-custom -->
			</div>
		</div>
	</div>
</div><!-- /.container -->
