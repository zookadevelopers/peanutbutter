	<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php if (is_single()) : ?>
	<meta name="description" content="<?php the_field('product_meta_description'); ?>"/>
	<link rel="canonical" href="<?php echo get_permalink(get_the_ID(), false); ?>" />
	<meta property="og:description" content="<?php the_field('product_meta_description'); ?>" />
	<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:site_name" content="Chi Chi's" />
	<meta property="og:title" content="<?php the_field('product-name'); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo get_permalink(get_the_ID(), false); ?>" />
	<meta property="article:published_time" content="<?php the_time('c'); ?>" />
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:description" content="<?php the_field('product_meta_description'); ?>" />
	<meta name="twitter:image" content="<?php the_post_thumbnail_url(); ?>">
	<meta name="twitter:title" content="<?php the_field('product-name'); ?>" />
	<?php endif ?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:500,700" />
</head>

<body <?php body_class(); ?>>
	<?php get_template_part( 'template-parts/mobile-menu' );?>

	<div class="loader d-none"><img src="<?php echo $upload_theme ?>logo.png" alt="SKIPPY® Brand Peanut Butter" /></div>
	<div id="page" class="site">
		<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?> main-header" role="banner">
			<div class="container">
				<nav class="navbar navbar-expand-md row nav-info">
					
					<div class="col-4 d-block d-md-none">
						<button class="ssm-toggle-nav hamburger-menu"><i class="fas fa-bars fa-2x"></i></button>
					</div><!-- /.col-12 col-2 -->

					<div class="col-4 col-md-2">
						<div class="navbar-brand">
							<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" class="img-fluid">
								</a>
							<?php else : ?>
								<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo $upload_theme; ?>logo.png" class="img-fluid" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
								</a>
							<?php endif; ?>

						</div>
					</div><!-- /.col-12 col-lg-4 -->
					<div class="col-6 d-none d-md-block">
						<?php
							wp_nav_menu(array(
								'theme_location'    => 'primary',
								'container'       => 'div',
								'container_id'    => 'main-nav',
								'container_class' => 'collapse navbar-collapse justify-content-center header-nav d-block',
								'menu_id'         => false,
								'menu_class'      => 'navbar-nav',
								'depth'           => 3,
								'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
								'walker'          => new wp_bootstrap_navwalker()
							));
						?>
					</div><!-- /.col-12 col-lg-8 -->
					<div class="col-4">
						<div class="row align-items-center">
							<div class="d-none d-md-block col-10">
								<?php get_search_form(); ?>
							</div><!-- /.col-6 -->
							<div class="d-block d-md-none col-6">
								<button type="submit" class="search-submit btn ssm-toggle-nav hamburger-menu search-open-menu" value="<?php echo esc_attr_x( 'Search', 'submit button', 'wp-bootstrap-starter' ); ?>"><i class="fas fa-search"></i></button>
							</div><!-- /.col-6 -->
							<div class="col p-0">
								<figure class="global-icon" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img class="img-fluid" src="<?php echo $upload_theme ?>/global/global-icon.svg" alt="global" width="auto" height="auto" />
								</figure>
								<div class="dropdown-menu drow-single-leng" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="http://www.peanutbutter.ca/">Canada</a>
									<a class="dropdown-item" href="http://www.skippychina.com/">China</a>
									<a class="dropdown-item" href="http://www.peanutbutter.id/">Indonesia</a>
									<a class="dropdown-item" href="http://www.peanutbutter.jpn.com/">Japan</a>
									<a class="dropdown-item" href="http://www.peanutbutter.mx/">Latin America</a>
									<a class="dropdown-item" href="http://www.peanutbutter.com.sg/">Singapore/Malaysia</a>
									<a class="dropdown-item" href="http://www.peanutbutter.se/">Sweden</a>
									<a class="dropdown-item" href="http://www.peanutbutter.uk.com/">United Kingdom</a>
									<a class="dropdown-item" href="./">United States</a>
								</div>		
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.col-12 col-lg-2 -->
				</nav>
			</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content pt-0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 p-0">
			