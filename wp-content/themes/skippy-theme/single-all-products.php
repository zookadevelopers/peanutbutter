<?php 
// Template Name: All Products


// all Products Query
$all_products = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
        'orderby'=>'menu_order'
	)
); 

// Products Query
$products_order = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
        'order' => 'ASC',
        'orderby'=>'menu_order'
	)
); 

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="cont-categories">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>all products</h1>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="products-home">
			<div class="row items-products items-all-products">
				<?php while ( $products_order->have_posts() ) : $products_order->the_post(); 
					if ( !in_array( "yes", get_field( 'hide_in_banner' ) ) ) { ?>
					<div class="col-6 col-sm-6 col-md-3 text-center item">
						<a class="link-product" href="<?php the_permalink(); ?>">
							<figure>
								<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
							</figure>
							<p class=""><?php the_title(); ?></p>
						</a>
					</div>
				<?php } endwhile; ?>
			</div><!-- /.row -->
		</div><!-- /.products-home -->
	</div><!-- /.container -->
</section><!-- /.title -->

<section class="products-home product-category all-product all-products-content">
	<div class="container">
		<?php $count = 1; while ( $all_products->have_posts() ) : 

			$all_products->the_post(); 
			$posSlug = get_field('post_type_slug');
			$posSlug = str_replace(" ","_",$posSlug);

			// one Product Query
			$one_product = new WP_Query(
				array(
					'post_type'			=> $posSlug, 
					'post_status'		=> 'publish', 
					'orderby'           => 'menu_order',
					'order'				=> 'ASC',
					'posts_per_page'	=> -1,
					'orderby'=>'menu_order'
				)
			);
		?>
			<div id="<?php echo $post->post_name; ?>" class="row name-product" data-count="<?php echo $count; ?>">
				<div class="col-12">
					<a href="<?php the_permalink(); ?>"><h3><?php the_field('title_category'); ?></h3></a>
				</div>
				<div class="col-12">
					<div class="description">
						<?php the_field('category_description'); ?>
					</div><!-- /.description -->
				</div>
			</div>
			<div class="row items-products list-product page-products">
				<?php while ( $one_product->have_posts() ) : $one_product->the_post(); ?>
					<?php
						$pageId = get_field('page_id');
						$name = get_the_title();
						$imgUrl = get_the_post_thumbnail_url();
						$currentUrl = home_url( $wp->request );						

						$objPowerReviews[] = [
							'api_key' => '7783bde7-0813-4233-8474-9c2889af0d40',
							'locale' => 'en_US',
							'merchant_group_id' => '78292',
							'merchant_id' => '146622',
							'page_id' => strval($pageId),
							'style_sheet' => get_site_url().'/wp-content/themes/skippy-theme/inc/assets/css/custom/reviews.css',
							'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
							'components' => [
								'ReviewSnippet' => 'pr-reviewsnippet-'.$pageId
							]
						];
					?>
					<?php get_template_part( 'template-parts/product-item' ); ?>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		<?php $count++; endwhile; wp_reset_postdata(); ?> 
	</div>
	<a class="back-top">
		<figure>
			<img class="img-fluid" src="<?php echo $upload_theme; ?>back-to-top.png" width="auto" height="auto" alt="Back to Top" />
		</figure>
	</a>
</section>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
