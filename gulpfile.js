/* Starter Gulpfile for Wordpress */

/* Libraries to use */
var gulp		 = require('gulp'),
	sass		 = require('gulp-sass'),
	minifyHTML	 = require('gulp-htmlmin'),
	minifyCSS	 = require('gulp-clean-css'),
	minifyJS	 = require('gulp-uglify'),
	imagemin	 = require('gulp-imagemin'),
	rename		 = require('gulp-rename'),
	prefix		 = require('gulp-autoprefixer'),
	size		 = require('gulp-size'),
	plumber		 = require('gulp-plumber'),
	spawn		 = require('child_process').spawn,
	shell        = require('gulp-shell'),
	watch		 = require('gulp-watch'),
	environments = require('gulp-environments');


/* Folder name template */
var templateName = 'skippy-theme',
	rootDirectory = 'wp-content/themes/'+templateName+'/';

/* Environments */
var development = environments.development,
	production = environments.production;

/* src and dist folders paths */
var paths = {
	src: {
		js: 'src/js',
		html: 'src/templates',
		scss: 'src/scss',
		fonts: 'src/fonts',
		img: 'src/img'
	},
	dest: {
		js: 'inc/assets/js',
		html: 'inc/assets/templates',
		css: 'inc/assets/css',
		img: 'inc/assets/img',
		fonts: 'inc/assets/fonts'
	}
};

/* Minify Html */
gulp.task('templating', function() {
	var options = {
		collapseWhitespace: true
	};

	gulp.src(rootDirectory+paths.src.html+'/**/*.html')
		.pipe(plumber())
		.pipe(production(minifyHTML(options)))
		.pipe(gulp.dest(rootDirectory+paths.dest.html));
});

/* Scss to css and Minify */
gulp.task('scss', function() {
	return gulp.src(rootDirectory+paths.src.scss+'/**/*.scss')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(prefix())
		.pipe(gulp.dest(rootDirectory+paths.dest.css))
		.pipe(production(size({ gzip: true, showFiles: true })))
		.pipe(production(rename({ suffix: '.min' })))
		.pipe(production(minifyCSS({compatibility: 'ie8'})))
		.pipe(gulp.dest(rootDirectory+paths.dest.css));
});

/* Minify JS */
gulp.task('js', function() {
	gulp.src(rootDirectory+paths.src.js+'/**/*.js')
		.pipe(plumber())
		.pipe(production(minifyJS()))
		.pipe(production(size({ gzip: true, showFiles: true })))
		.pipe(gulp.dest(rootDirectory+paths.dest.js));
});

/* Fonts */
gulp.task('fonts', function() {
	return gulp.src(rootDirectory+paths.src.fonts+'/**/*')
		.pipe(gulp.dest(rootDirectory+paths.dest.fonts));
});

/* Image compression modules */
gulp.task('imagemin', function(){
	gulp.src([rootDirectory+paths.src.img+'/**/*'])
		.pipe(production(imagemin()))
		.pipe(gulp.dest(rootDirectory+paths.dest.img))
});


/* Linters */
gulp.task('base-lint', shell.task('/runner/./check-base.sh --no-exit', {ignoreErrors: true}));
gulp.task('sass-lint', shell.task('/runner/./check-sass.sh --no-exit', {ignoreErrors: true}));
gulp.task('html-lint', shell.task('/runner/./check-html.sh --no-exit', {ignoreErrors: true}));
gulp.task('js-lint',   shell.task('/runner/./check-js.sh --no-exit', {ignoreErrors: true}));

/* Look for changes on files */
gulp.task('watch', function() {

	gulp.watch(rootDirectory+paths.src.html+'/**/*.html', 	['templating', 'html-lint', 'base-lint']);
	gulp.watch(rootDirectory+paths.src.scss+'/**/*.scss', ['scss', 'sass-lint', 'base-lint']);
	gulp.watch(rootDirectory+paths.src.js+'/**/*.js', 		['js', 'js-lint', 'base-lint']);
	gulp.watch(rootDirectory+paths.src.img+'/**/*', 		['imagemin', 'base-lint']);
	gulp.watch(rootDirectory+paths.src.fonts+'/**/*', 	['fonts', 'base-lint']);

});

/* run tasks */
gulp.task('default', [
	'templating',
	'scss',
	'js',
	'imagemin',
	'fonts',
	'watch',
	'base-lint',
	'html-lint',
	'sass-lint',
	'js-lint'
]);
