#Project into zookastaging
updated by Steve on 5/19/19

##Clone repo
git clone https://stevedecker@bitbucket.org/zookadevelopers/skippy.git

after cloning, we must change the mysql password in the **wp-config.php** file, for that we run this command

`cd skippy`
`sudo nano wp-config.php`

below the comment that says **_/* MySQL database password */_** we change the password `define('DB_PASSWORD', '6Wct5U9g9BvMXwjH');`

the phpmyadmin password of the server is **6Wct5U9g9BvMXwjH**

Now, if we enter the url [http://zookastaging.com/skippy](http://zookastaging.com/skippy "http://zookastaging.com/skippy") we will see the site in live, but the other links will not work, the solution is to create a .htaccess file 

```
<IfModule mod_rewrite.c>
        RewriteEngine On
        RewriteBase /skippy/
        RewriteRule ^index\.php$ - [L]
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule . /skippy/index.php [L]
</IfModule>
```

##Upload database

Update the ip/dns in the backup file of the database.

`replace "skippy.zookastaging.com" "localhost" --  admin_skippy.sql`

Login to mysql with your username and password `mysql -u root -p` and in the mysql console type the following command` create database skippy`, logout to mysql, then import the database `mysql skippy < [DATABASE-BACKUP-DIRECTORY]`.

or you can connect by db-adminer which is a database manager and create and import the database, to connect enter through the url *[http://35.236.54.192/skippy/db.adminer/](http://35.236.54.192/skippy/db.adminer/ "http://35.236.54.192/skippy/db.adminer/")* and complete the conexion data

