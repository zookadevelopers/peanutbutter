# Make container
# docker run -ti --name skippy -p 80:80 -p 3306:3306 -v $(PWD):/var/www/html -v ~/docker/skippy:/var/lib/mysql registry.gitlab.com/sigma-studios/skippy

# Start from Ubuntu 18.04 LTS
From ubuntu:18.04

# Declare ENV Vars
ENV MYSQL_PASSWORD root_password
ENV DEBIAN_FRONTEND noninteractive
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

# Install required packages #Wordpress
RUN apt-get update && apt-get -y install \
	wget \
	unzip \
	curl \
	nginx \
	php \
	php-fpm \
	php-mbstring \
	php-mysql \
	php-xml \
	php-curl \
	php-zip \
	sudo \
	nano \
	vim \
	nodejs \
	npm \
	git \
	zip

# Install Npm global modules
RUN npm install -g gulp \
	&& npm install -g htmllint-cli \
	&& npm install -g sass-lint \
	&& npm install -g jshint \
	&& npm install -g bower

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php \
	&& mv composer.phar /usr/local/bin/composer

# Remove cmdtest for installing Yarn
RUN sudo apt remove cmdtest
RUN sudo apt-get update && sudo apt-get -y install yarn

# Install MySql
RUN echo "mysql-server mysql-server/root_password password $MYSQL_PASSWORD" | debconf-set-selections
RUN echo "mysql-server mysql-server/root_password_again password $MYSQL_PASSWORD" | debconf-set-selections
RUN apt-get -y install mysql-server
RUN sudo chown -R mysql:mysql /var/lib/mysql

# Add configuration files
ADD builder/requirements/default /etc/nginx/sites-available/default
ADD builder/requirements/php.ini /etc/php/7.2/fpm/php.ini
ADD builder/requirements/.htmllintrc /var/www/html/.htmllintrc

# Add scripts
ADD builder/runner /runner
RUN chmod +x /runner/*.sh

# Set workdir
WORKDIR /var/www/html/wp-content/themes

# Expose
EXPOSE 80 3306
