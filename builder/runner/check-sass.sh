#!/bin/bash

#COLOR VARS
START_INFO_COLOR='\033[0;32m'
END_INFO_COLOR='\033[0m'
#THEME FOLDER
TEMPLATE_NAME=skippy-theme
PROJECT_DIR=/var/www/html/wp-content/themes/${TEMPLATE_NAME}/src/scss/custom

if [ "$1" == "--no-exit" ]; then

	WARNINGS=0

	for FILE in $(find $PROJECT_DIR -type f \( -name '*.sass' -or -name '*.scss' \));do
		echo -e ${START_INFO_COLOR}$FILE${END_INFO_COLOR};
		if ! sass-lint -c /runner/.sass-lint.yml -q -v --max-warnings 0 --no-exit $FILE;then
			WARNINGS=1;
		fi;
	done;

	if [ "$WARNINGS" = "0" ];then
		echo -e "${START_INFO_COLOR}The SASS files satisfy every linter rules${END_INFO_COLOR}";
	fi;

else

	ERRORS=0

	for FILE in $(find $PROJECT_DIR -type f \( -name '*.sass' -or -name '*.scss' \));do
		echo -e ${START_INFO_COLOR}$FILE${END_INFO_COLOR};
		if ! sass-lint -c /runner/.sass-lint.yml -q -v --max-warnings 0 $FILE;then
			ERRORS=1;
		fi;
	done;

	if [ "$ERRORS" = "0" ];then
		echo -e "${START_INFO_COLOR}The SASS files satisfy every linter rules${END_INFO_COLOR}";
	fi;

	exit $ERRORS

fi;