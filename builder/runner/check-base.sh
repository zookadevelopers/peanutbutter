#!/bin/bash

#COLOR VARS
START_INFO_COLOR='\033[0;32m'
END_INFO_COLOR='\033[0m'
#THEME FOLDER
TEMPLATE_NAME=skippy-theme
PROJECT_DIR=/var/www/html/wp-content/themes/${TEMPLATE_NAME}

ERRORS=0

# Check no uppercase characters are used in the filenames
if find $PROJECT_DIR | grep -E '.*[A-Z].*'; then
	echo -e "${START_INFO_COLOR}Filenames must always be lowercase.${END_INFO_COLOR}"
	ERRORS=1;
fi

if [ "$ERRORS" = "0" ];then
	echo -e "${START_INFO_COLOR}All files satisfy every linter rules${END_INFO_COLOR}";
fi;

if [ !$1 ];then
	exit $ERRORS
fi;