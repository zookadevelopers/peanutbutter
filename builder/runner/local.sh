#!/bin/bash

#COLOR VARS
START_INFO_COLOR='\033[0;32m'
END_INFO_COLOR='\033[0m'
#THEME FOLDER
PROJECT_DIR=/var/www/html

if [ -z "$(ls -A /var/lib/mysql)" ]; then
	mysqld --initialize-insecure
fi

echo -e "${START_INFO_COLOR}Starting LEMP...${END_INFO_COLOR}";

# START SERVICES
sudo service php7.2-fpm start
sudo service mysql start
sudo service nginx start

echo -e "${START_INFO_COLOR}Services successfully started...${END_INFO_COLOR}";

# GO TO WORKDIR
cd $PROJECT_DIR

echo -e "${START_INFO_COLOR}Starting Gulp...${END_INFO_COLOR}";

# Star npm
npm install
gulp


#KEEP BASH RUNNING
tail -f /dev/null
