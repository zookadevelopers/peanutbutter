#!/bin/bash

#COLOR VARS
START_INFO_COLOR='\033[0;32m'
END_INFO_COLOR='\033[0m'
#THEME FOLDER
TEMPLATE_NAME=skippy-theme
PROJECT_DIR=/var/www/html/wp-content/themes/${TEMPLATE_NAME}/src/js/custom

##########

ERRORS=0

for FILE in $(find $PROJECT_DIR -type f \( -name '*.js' \));do
	echo -e ${START_INFO_COLOR}$FILE${END_INFO_COLOR};
	if ! jshint $FILE;then
		ERRORS=1;
	fi;
done;

if [ "$ERRORS" = "0" ];then
	echo -e "${START_INFO_COLOR}The JS files satisfy every linter rules${END_INFO_COLOR}";
fi;

if [ !$1 ];then
	exit $ERRORS
fi;